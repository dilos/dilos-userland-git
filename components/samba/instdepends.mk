INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += dh-python
# dh-systemd (>= 1.5),
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
#INSTDEPENDS += faketime
INSTDEPENDS += flex
# libacl1-dev
# libaio-dev [linux-any],
INSTDEPENDS += libaio-dev
INSTDEPENDS += libarchive-dev
# libattr1-dev
INSTDEPENDS += libblkid-dev
###INSTDEPENDS += libbsd-dev
# libcap-dev [linux-any],
INSTDEPENDS += libcups2-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libgpgme11-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libldb-dev
INSTDEPENDS += libncurses5-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libparse-yapp-perl
INSTDEPENDS += libpcap-dev
INSTDEPENDS += libpopt-dev
INSTDEPENDS += libreadline-dev
# libsystemd-dev [linux-any],
INSTDEPENDS += libtalloc-dev
INSTDEPENDS += libtdb-dev
INSTDEPENDS += libtevent-dev
# perl
INSTDEPENDS += pkg-config
INSTDEPENDS += po-debconf
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-dnspython
INSTDEPENDS += python-ldb
INSTDEPENDS += python-ldb-dev
INSTDEPENDS += python-talloc-dev
INSTDEPENDS += python-tdb
INSTDEPENDS += python-testtools
INSTDEPENDS += python3
INSTDEPENDS += xsltproc
INSTDEPENDS += zlib1g-dev
