INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += gettext
# libbsd-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libidn11-dev
INSTDEPENDS += libncursesw5-dev
# openssh-client
INSTDEPENDS += sharutils
# telnet | telnet-client
INSTDEPENDS += zlib1g-dev
