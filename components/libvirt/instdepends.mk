INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
# dh-systemd (>= 1.18~),
# dh-apparmor [linux-any],
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libgcrypt20-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libavahi-client-dev
INSTDEPENDS += libsasl2-dev
# libxen-dev (>= 4.3) [i386 amd64 armhf arm64],
# lvm2 [linux-any],
# open-iscsi [linux-any],
# libparted0-dev (>= 2.2),
# parted (>= 2.2),
# libdevmapper-dev [linux-any],
# uuid-dev,
INSTDEPENDS += libuuid-dev
# libudev-dev [linux-any],
INSTDEPENDS += libpciaccess-dev
# kmod [linux-any],
INSTDEPENDS += policykit-1
INSTDEPENDS += libpolkit-gobject-1-dev
# libcap-ng-dev [linux-any],
# libnl-3-dev [linux-any],
# libnl-route-3-dev [linux-any],
INSTDEPENDS += libyajl-dev
INSTDEPENDS += libpcap0.8-dev
# libnuma-dev [amd64 arm64 i386 ia64 mips mipsel powerpc ppc64 ppc64el],
# numad [amd64 arm64 i386 ia64 mips mipsel powerpc ppc64 ppc64el],
# radvd [linux-any],
# libnetcf-dev (>= 1:0.2.3-3~) [linux-any],
# libsanlock-dev [linux-any],
# libaudit-dev [linux-any],
# libselinux1-dev (>= 2.0.82) [linux-any],
# libapparmor-dev [linux-any],
# libdbus-1-dev [linux-any],
# nfs-common,
# systemtap-sdt-dev [amd64 armel armhf i386 ia64 powerpc s390],
INSTDEPENDS += python
INSTDEPENDS += xsltproc
# zfsutils [kfreebsd-amd64 kfreebsd-i386],
INSTDEPENDS += po-debconf
# for --with-storage-sheepdog
# sheepdog [linux-any],
# for --with-storage-rados
# librbd-dev [linux-any],
# librados-dev [linux-any],
# for lxc fuse support
# libfuse-dev [linux-any],
# for libssh2 connection URIs
INSTDEPENDS += libssh2-1-dev
# for qemu-bridge-helper
#INSTDEPENDS += qemu-system-common
# For "make check"
# augeas-tools
# dwarves
INSTDEPENDS += libxml2-utils
# dnsmasq-base
# openssh-client
# netcat-openbsd,
# ebtables [linux-any],
# iptables [linux-any],
# qemu-utils
