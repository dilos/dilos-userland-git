INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += bsdmainutils
INSTDEPENDS += cm-super-minimal
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += ghostscript
INSTDEPENDS += groff
INSTDEPENDS += groff-base
# libcap-dev [linux-any]
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += texinfo
INSTDEPENDS += texlive-fonts-recommended
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += texlive-latex-recommended
INSTDEPENDS += yodl
# (>= 3.08.01) | yodl (<< 3.08.00)
