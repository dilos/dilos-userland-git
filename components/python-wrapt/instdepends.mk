INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += openstack-pkg-tools
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
# python-sphinx
# python-sphinx-rtd-theme
INSTDEPENDS += python3-all
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-six
