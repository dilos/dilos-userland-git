INSTDEPENDS += debhelper
INSTDEPENDS += apache2-dev
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += bison
# chrpath
INSTDEPENDS += default-libmysqlclient-dev
# | libmysqlclient-dev
#INSTDEPENDS += libmysqlclient-dev
#INSTDEPENDS += default-mysql-server | mysql-server | virtual-mysql-server,
INSTDEPENDS += dh-apache2
INSTDEPENDS += dpkg-dev
# firebird-dev [!hurd-any !m68k !hppa !ppc64] | firebird2.5-dev [!hurd-any !m68k !hppa !ppc64] | firebird2.1-dev [!hurd-any !m68k !hppa !ppc64],
### firebird-dev
# [!hurd-any !m68k !hppa !ppc64] | firebird2.5-dev [!hurd-any !m68k !hppa !ppc64] | firebird2.1-dev [!hurd-any !m68k !hppa !ppc64],
INSTDEPENDS += flex
INSTDEPENDS += freetds-dev
INSTDEPENDS += libapr1-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libc-client-dev
INSTDEPENDS += libcurl4-openssl-dev
# | libcurl-dev,
INSTDEPENDS += libdb-dev
INSTDEPENDS += libedit-dev
INSTDEPENDS += libenchant-dev
INSTDEPENDS += libevent-dev
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += libgcrypt11-dev
INSTDEPENDS += libgd-dev
# (>= 2.1.0) | libgd2-dev,
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libgmp3-dev
INSTDEPENDS += libicu-dev
INSTDEPENDS += libjpeg-dev
# | libjpeg62-dev,
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libmagic-dev
INSTDEPENDS += libmcrypt-dev
INSTDEPENDS += libmhash-dev
INSTDEPENDS += libonig-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libpq-dev
INSTDEPENDS += libpspell-dev
INSTDEPENDS += libqdbm-dev
INSTDEPENDS += librecode-dev
INSTDEPENDS += libsasl2-dev
INSTDEPENDS += libsnmp-dev
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libtidy-dev
INSTDEPENDS += libtool
INSTDEPENDS += libwebp-dev
INSTDEPENDS += libwrap0-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxmlrpc-epi-dev
INSTDEPENDS += libxmltok1-dev
INSTDEPENDS += libxslt1-dev
INSTDEPENDS += libzip-dev
# locales-all | language-pack-de,
# netbase
INSTDEPENDS += netcat-traditional
INSTDEPENDS += re2c
#INSTDEPENDS += systemtap-sdt-dev [amd64 i386 powerpc armel armhf ia64 linux],
# tzdata
INSTDEPENDS += unixodbc-dev
INSTDEPENDS += zlib1g-dev
#
INSTDEPENDS += libnghttp2-dev
INSTDEPENDS += librtmp-dev
INSTDEPENDS += libidn2-0-dev
INSTDEPENDS += libssh2-1-dev
INSTDEPENDS += libpsl-dev
