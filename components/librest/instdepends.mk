INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
INSTDEPENDS += automake
# ca-certificates
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gir1.2-freedesktop
INSTDEPENDS += gir1.2-glib-2.0
INSTDEPENDS += gobject-introspection
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libsoup-gnome2.4-dev
INSTDEPENDS += libsoup2.4-dev
INSTDEPENDS += libtool
INSTDEPENDS += libxml2-dev
