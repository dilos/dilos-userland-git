lzip (1.18-1) unstable; urgency=low

  * Uploading to sid.
  * Merging upstream version 1.18.
  * Updating vcs fields.
  * Removing manual settings for xz compression.
  * Updating years in copyright file.
  * Harmonizing rules file.
  * Updating to standards version 3.9.8.
  * Wrap and sorting control file.
  * Updating years in copyright file.
  * Switching to debhelper compat 10.
  * Adding watch file.
  * Refreshing build.patch.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 03 Jun 2016 17:52:30 +0200

lzip (1.18~pre1-1) experimental; urgency=low

  * Merging upstream version 1.18~pre1.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 14 Aug 2015 07:57:27 +0200

lzip (1.17-1) unstable; urgency=low

  * Merging upstream version 1.17.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 28 Jul 2015 10:02:47 +0200

lzip (1.17~rc2-1) experimental; urgency=low

  * Merging upstream version 1.17~rc2.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 01 Jun 2015 08:01:09 +0200

lzip (1.17~rc1-1) experimental; urgency=low

  * Merging upstream version 1.17~rc1.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 24 Apr 2015 22:39:02 +0200

lzip (1.17~pre1-1) experimental; urgency=low

  * Merging upstream version 1.17~pre1.
  * Updating years in copyright file.
  * Wrapping build-depends.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 06 Apr 2015 22:44:58 +0200

lzip (1.16-2) unstable; urgency=low

  * Updating to standards version 3.9.6.
  * Harmonizing update-alternatives handling across all lzip-alternative
    providing packages.
  * Harmonizing update-alternatives priority across all lzip-alternative
    providing packages.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 18 Sep 2014 00:14:54 +0200

lzip (1.16-1) unstable; urgency=low

  * Updating copyright file.
  * Merging upstream version 1.16.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 09 Sep 2014 19:40:20 +0200

lzip (1.16~rc1-1) unstable; urgency=low

  * Merging upstream version 1.16~rc1.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 05 Jul 2014 08:47:19 +0200

lzip (1.16~pre2-1) unstable; urgency=low

  * Merging upstream version 1.16~pre2.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 26 May 2014 22:33:43 +0200

lzip (1.16~pre1-2) unstable; urgency=low

  * Updating year in copyright file.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 31 Mar 2014 21:11:40 +0200

lzip (1.16~pre1-1) experimental; urgency=low

  * Updating to standards version 3.9.5.
  * Merging upstream version 1.16~pre1.
  * Updating years in copyright.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 08 Feb 2014 15:44:10 +0100

lzip (1.15-1) experimental; urgency=low

  * Merging upstream version 1.15.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 01 Oct 2013 20:48:13 +0200

lzip (1.15~rc1-1) experimental; urgency=low

  * Updating vcs fields.
  * Merging upstream version 1.15~rc1.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 12 Aug 2013 13:32:07 +0200

lzip (1.15~pre3-1) experimental; urgency=low

  * Merging upstream version 1.15~pre3.
  * Rediffing build.patch.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 17 Jul 2013 18:46:09 +0200

lzip (1.15~pre2-4) experimental; urgency=low

  * Adding vcs fields.
  * Wrapping control fields.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 17 Jul 2013 13:29:40 +0200

lzip (1.15~pre2-3) experimental; urgency=low

  * Correcting lzip-dbg package short-description.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 11 Jun 2013 17:51:07 +0200

lzip (1.15~pre2-2) experimental; urgency=low

  * Adding lzip-alternative provides.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 04 Jun 2013 16:55:20 +0200

lzip (1.15~pre2-1) experimental; urgency=low

  * Merging upstream version 1.15~pre2.
  * Correcting spelling typo in comment in rules.
  * Sorting overrides in rules.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 16 May 2013 10:19:29 +0200

lzip (1.15~pre1-1) experimental; urgency=low

  * Correcting email address in previous changelog entry.
  * Merging upstream version 1.15~pre1.
  * Updating years in copyright file.
  * Dropping dpkg-source compression levels.
  * Prefixing patches with four digits.
  * Trimming diff headers in patches.
  * Adding alternative handling for /usr/bin/lzip.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 25 Mar 2013 15:33:04 +0100

lzip (1.14-2) unstable; urgency=low

  * Removing all references to my old email address.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 10 Mar 2013 21:07:00 +0100

lzip (1.14-1) unstable; urgency=low

  * Merging upstream version 1.14.
  * Rediffing build.patch.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 10 Mar 2013 21:02:15 +0100

lzip (1.13-4) unstable; urgency=low

  * Updating to standards version 3.9.4.
  * Adding dpkg-source local options.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 16 Dec 2012 10:41:23 +0100

lzip (1.13-3) unstable; urgency=low

  * Correcting wrong upstream name in copyright file.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 30 Jun 2012 16:11:15 +0200

lzip (1.13-2) unstable; urgency=low

  * Correcting email address in changelog of version 1.13~rc2-1.
  * Adding patch to avoid overwriting build-environment.
  * Switching to xz compression.
  * Using license exception description in license field in copyright
    file.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 30 Jun 2012 00:58:24 +0200

lzip (1.13-1) unstable; urgency=low

  * Merging upstream version 1.13.
  * Updating to debhelper version 9.
  * Updating to standards version 3.9.3.
  * Updating copyright file machine-readable format version 1.0.
  * Adding plzip to suggests.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 13 Mar 2012 20:26:54 +0100

lzip (1.13~rc2-1) unstable; urgency=low

  * Merging upstream version 1.13~rc2.
  * Updating years in copyright file.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 08 Jan 2012 10:58:17 +0100

lzip (1.13~rc1-1) unstable; urgency=low

  * Using compression level 9 also for binary packages.
  * Merging upstream version 1.13~rc1.
  * Adding lziprecover to suggests.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 15 Nov 2011 00:09:52 +0100

lzip (1.12-2) unstable; urgency=low

  * Compacting copyright file.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 10 Jul 2011 17:09:49 +0200

lzip (1.12-1) unstable; urgency=low

  * Updating package descriptions.
  * Updating standards version to 3.9.2.
  * Merging upstream version 1.12.
  * Updating year in copyright files.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 16 May 2011 21:49:48 +0200

lzip (1.11-5) unstable; urgency=low

  * Making reference to licenses in copyright distribution neutral.
  * Improving previous changelog entries.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 22 Jan 2011 21:33:03 +0100

lzip (1.11-4) unstable; urgency=low

  * Updating maintainer and uploaders fields.
  * Removing vcs fields.
  * Removing references to my old email address.
  * Makeing packaging distribution neutral.
  * Improving comments in rules file.
  * Simplyfing debhelper auto_install override in rules.
  * Switching to source format 3.0 (quilt).
  * Removing local lunzip wrapper in favour of upstreams own lunzip.
  * Updating compat for debhelper version 8.
  * Updating year in copyright file.
  * Removing pre-squeeze dpkg depends.
  * Updating copyright file.
  * Removing unneeded build-depends on autotools-dev.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 13 Jan 2011 23:00:51 +0100

lzip (1.11-3) experimental; urgency=low

  * Updating lunzip wrapper script.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 27 Oct 2010 23:12:20 +0200

lzip (1.11-2) experimental; urgency=low

  * Adding lunzip wrapper.
  * Updating to debhelper version 8.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 14 Oct 2010 22:43:25 +0200

lzip (1.11-1) experimental; urgency=low

  * Updating standards version to 3.9.0.
  * Merging upstream version 1.11.
  * Updating standards version to 3.9.1.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 25 Sep 2010 17:44:52 +0200

lzip (1.10-1) unstable; urgency=low

  * Updating to standards 3.8.4.
  * Merging upstream version 1.10.
  * Updating README.source.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 09 Apr 2010 11:56:14 +0200

lzip (1.9-1) unstable; urgency=low

  * Bumping versioned build-depends on debhelper.
  * Adding explicit debian source version 1.0 until switch to 3.0.
  * Updating year in copyright file.
  * Merging upstream version 1.9.
  * Updating year in copyright file.
  * Adding depends on dpkg and install-info.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 18 Jan 2010 21:45:35 +0100

lzip (1.8-1) unstable; urgency=low

  * Updating package to standards version 3.8.3.
  * Adding maintainer homepage field to control.
  * Marking maintainer homepage field to be also included in binary
    packages and changelog.
  * Adding README.source.
  * Merging upstream version 1.8.
  * Moving maintainer hompage entry from control to copyright.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 04 Sep 2009 21:58:19 +0200

lzip (1.7-3) unstable; urgency=low

  * Updating maintainer field.
  * Updating vcs fields.
  * Commenting rules file.
  * Adding misc depends.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 14 Aug 2009 02:16:06 +0200

lzip (1.7-2) unstable; urgency=low

  * Minimizing rules file, which also dropps not required autotools foo
    (Closes: #533713).
  * Sorting depends.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 09 Aug 2009 11:10:02 +0200

lzip (1.7-1) unstable; urgency=low

  * Merging upstream version 1.7.
  * Updating standards version to 3.8.2.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 06 Jul 2009 13:59:50 +0200

lzip (1.6-1) unstable; urgency=low

  * Correcting wrong email address in previous changelog entry.
  * Merging upstream version 1.6.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 06 Jul 2009 13:54:00 +0200

lzip (1.6~pre3-1) unstable; urgency=low

  * Merging upstream version 1.6~pre3:
    - Fixes bashism in included shell scripts (Closes: #530135).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 08 Jun 2009 18:14:50 +0200

lzip (1.6~pre2-1) unstable; urgency=low

  * Merging upstream version 1.6~pre2.
  * Removing dir info pages, apparently useless (Closes: #530619).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 28 May 2009 23:42:44 +0200

lzip (1.6~pre1-1) unstable; urgency=low

  * Initial release.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 30 Apr 2009 15:31:00 +0200
