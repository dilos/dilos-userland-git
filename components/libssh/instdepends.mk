INSTDEPENDS += debhelper
INSTDEPENDS += cmake
INSTDEPENDS += libcmocka-dev
INSTDEPENDS += libgcrypt-dev
INSTDEPENDS += libkrb5-dev
# | heimdal-dev,
INSTDEPENDS += libssl1.0-dev
INSTDEPENDS += libz-dev
# openssh-client
INSTDEPENDS += pkg-config
#Build-Depends-Indep:
INSTDEPENDS += doxygen
