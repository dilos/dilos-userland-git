INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
# dbus
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libxtst-dev
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += intltool
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += gobject-introspection
