INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
# lsb-release,
#INSTDEPENDS += bzip2
# Other tool deps
INSTDEPENDS += autoconf
INSTDEPENDS += libtool
INSTDEPENDS += gettext
INSTDEPENDS += bison
INSTDEPENDS += dejagnu
INSTDEPENDS += flex
# procps
# Do we really care that much about running the Java tests?
# gcj-jdk | gcj,
# gobjc,
# mig [hurd-any],
# TeX[info] deps
INSTDEPENDS += texinfo
# texlive-base,
# Libdev deps
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += liblzma-dev
# libbabeltrace-dev [amd64 armel armhf i386 kfreebsd-amd64 kfreebsd-i386 mips mipsel powerpc s390x]
# libbabeltrace-ctf-dev [amd64 armel armhf i386 kfreebsd-amd64 kfreebsd-i386 mips mipsel powerpc s390x]
INSTDEPENDS += python-dev
INSTDEPENDS += python3-dev
