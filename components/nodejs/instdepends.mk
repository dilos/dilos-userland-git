INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
# devscripts
INSTDEPENDS += dh-buildinfo
# binutils
# openssl
INSTDEPENDS += pkg-config
# bash-completion
# curl
# procps
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libicu-dev
INSTDEPENDS += libkvm-dev
INSTDEPENDS += gyp
# ca-certificates
# node-yamlish <!stage1>,
# node-marked <!stage1>,
INSTDEPENDS += python
INSTDEPENDS += libssl1.0-dev
INSTDEPENDS += libuv1-dev
#
INSTDEPENDS += developer-dtrace
INSTDEPENDS += libsendfile-dev
