INSTDEPENDS += debhelper
INSTDEPENDS += fastjar
# default-jdk
# file
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libcroco3-dev
INSTDEPENDS += xz-utils
INSTDEPENDS += libunistring-dev
