INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += xsltproc
INSTDEPENDS += docbook-xsl
INSTDEPENDS += libx11-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += xutils-dev
INSTDEPENDS += libpthread-stubs0-dev
# libudev-dev [linux-any],
INSTDEPENDS += libpciaccess-dev
# valgrind [amd64 armhf i386 mips mipsel powerpc s390x],
# libbsd-dev [kfreebsd-any],
