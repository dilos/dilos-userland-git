INSTDEPENDS += debhelper
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
INSTDEPENDS += xfonts-utils
# DEB_HOST_MULTIARCH, dpkg-buildflags --export=configure
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dh-autoreconf
