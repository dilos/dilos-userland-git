INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dh-python
# pypy
# pypy-hypothesis <!nocheck>
# pypy-setuptools
# pypy-py
INSTDEPENDS += python-all
# python-doc
# python-mock (>= 1.0.1) <!nocheck>,
# python-hypothesis <!nocheck>,
# python-nose <!nocheck>,
# python-pexpect <!nocheck>,
# python-py (>= 1.4.29),
INSTDEPENDS += python-setuptools
# python-sphinx (>= 1.0.7+dfsg),
# python-twisted-core <!nocheck>,
INSTDEPENDS += python3-all
# python3-hypothesis <!nocheck>,
# python3-mock (>= 1.0.1) <!nocheck>,
# python3-nose <!nocheck>,
# python3-py (>= 1.4.29),
INSTDEPENDS += python3-setuptools
