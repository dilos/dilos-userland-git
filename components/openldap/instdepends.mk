INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += groff-base
INSTDEPENDS += heimdal-multidev
INSTDEPENDS += libdb5.3-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libltdl-dev
INSTDEPENDS += libperl-dev
INSTDEPENDS += libsasl2-dev
#INSTDEPENDS += libsasl-dev
INSTDEPENDS += libslp-dev
INSTDEPENDS += libwrap0-dev
INSTDEPENDS += nettle-dev
# perl
INSTDEPENDS += po-debconf
# time <!stage1>
INSTDEPENDS += unixodbc-dev
