INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libboost-dev
INSTDEPENDS += libboost-date-time-dev
INSTDEPENDS += libboost-regex-dev
INSTDEPENDS += libboost-test-dev
