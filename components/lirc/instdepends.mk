INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += dh-python
INSTDEPENDS += doxygen
# libasound2-dev [linux-any kfreebsd-any],
INSTDEPENDS += libftdi1-dev
# libusb-1.0-0-dev [!kfreebsd-any],
INSTDEPENDS += libusb-1.0-0-dev
# libusb-dev[!kfreebsd-any],
INSTDEPENDS += libusb-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += man2html-base
INSTDEPENDS += pkg-config
INSTDEPENDS += portaudio19-dev
INSTDEPENDS += python3
INSTDEPENDS += python3-yaml
INSTDEPENDS += xsltproc
#
INSTDEPENDS += libuuid-dev
