INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
# python-sphinx (>= 1.0.7+dfsg) | python3-sphinx
INSTDEPENDS += python-pygments
