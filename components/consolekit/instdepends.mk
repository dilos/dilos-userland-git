INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += libdbus-glib-1-dev
INSTDEPENDS += libglib2.0-dev
# libacl1-dev [linux-any],
# libudev-dev [linux-any],
INSTDEPENDS += libx11-dev
# libkvm-dev [kfreebsd-any],
INSTDEPENDS += xmlto
# libpam0g-dev,
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpolkit-gobject-1-dev
INSTDEPENDS += zlib1g-dev
# libbsd-dev (>= 0.3.0~) [kfreebsd-any],
INSTDEPENDS += dh-autoreconf
