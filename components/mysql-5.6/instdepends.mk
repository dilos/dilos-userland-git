INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += chrpath
INSTDEPENDS += cmake
INSTDEPENDS += dpkg-dev
INSTDEPENDS += gawk
INSTDEPENDS += libedit-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libwrap0-dev
# lsb-release
# perl
INSTDEPENDS += po-debconf
# psmisc
INSTDEPENDS += zlib1g-dev
#
INSTDEPENDS += dtrace
INSTDEPENDS += libmtmalloc-dev
