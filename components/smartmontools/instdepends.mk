INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
INSTDEPENDS += automake1.11
INSTDEPENDS += dh-exec
# dh-systemd (>= 1.13),
# freebsd-glue [kfreebsd-any],
# libcam-dev [kfreebsd-any],
# libcap-ng-dev [!kfreebsd-any !hurd-i386 !sparc !avr32],
# libselinux1-dev [linux-any],
# libusb2-dev [kfreebsd-any]
