INSTDEPENDS += debhelper
INSTDEPENDS += libltdl-dev
#  valgrind [amd64 arm64 armhf i386 mips mipsel powerpc ppc64el s390x mips64el ppc64],
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxml2-utils
INSTDEPENDS += libncurses5-dev
#  libnuma-dev [amd64 arm64 i386 ia64 mips mips64 mipsel mips64el powerpc ppc64el sparc],
#  libpciaccess-dev
INSTDEPENDS += pkg-config
#  libibverbs-dev [linux-any]
#  ocl-icd-opencl-dev [!hurd-i386] | opencl-dev
# opencl-headers
INSTDEPENDS +=  autoconf
INSTDEPENDS +=  dh-autoreconf
INSTDEPENDS +=  dpkg-dev
#Build-Depends-Indep: doxygen-latex, transfig
