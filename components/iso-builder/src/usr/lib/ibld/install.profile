

DESKTOP_CMN_DEBS="x-window-system-core menu mime-support \
    gdm gnome-session gnome-panel gnome-terminal gksu \
    gnome-desktop-environment nautilus metacity synaptic evolution gaim \
    firefox-gnome-support nexenta-artwork nexenta-sounds gnome-cups-manager \
    gs gs-common gs-gpl xterm firefox update-notifier hwdb-client gdebi \
    gnome-spell aspell-en totem-gstreamer-firefox-plugin"

LOCAL_ARCHIVE_DEBS=" \
    bastet screen dialog ${DESKTOP_CMN_DEBS} libgnome2-perl \
    sunwtnetc sunwesu sunwtoo sunwsshu sunwsshdu sunwpppg \
    vim vim-gnome sunwjre1.5 nevada-compat openoffice.org2-monolith \
    cupsys-driver-gutenprint libpaper1 \
    gnome xcursor-themes gnome-app-install alacarte \
    gstreamer0.10-sunaudio gimp inkscape xchat-gnome gnome-btdownload \
    sunwpppgs sunwpppd sunwpppdr sunwpppdt sunwpppdu sunwpppg \
    sunwsn1rint sunwsn1uint sunwlxr sunwlxu \
    sunwnisr sunwnisu \
    sunwsndmr sunwsndmu \
    alien nexenta-pkgcmd \
    sunwscpu \
    gstreamer0.10-x gstreamer0.10-plugins-base gstreamer0.10-plugins-good gstreamer0.10-gnomevfs \
    evolution-data-server powermanagement-interface libtotem-plparser1 \
    gedit-common capplets-data gnome-desktop-data libpoppler1-glib libtotem-plparser1 \
    gnome-doc-utils system-tools-backends gnome-panel-data gnome-desktop-data \
    gstreamer0.10-plugins-ugly gstreamer0.10-ffmpeg gstreamer0.10-tools \
    samba samba-common winbind smbclient ${REQ_DEBS}"
