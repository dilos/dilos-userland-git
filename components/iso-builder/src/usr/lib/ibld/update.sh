#!/bin/bash

test ! -f /etc/ibld.cf && echo "Error: can't find /etc/ibld.cf" && exit 1
. /etc/ibld.cf

echo "Updating components:"

#echo "Updating $IBLD_MINIROOT/kernel/drv/sd.conf ..."
#echo "pm-capable=0;" >> $IBLD_MINIROOT/kernel/drv/sd.conf
#echo "sd-config-list = \"SEAGATE ST9146853SS\", \"power-condition:false\";" >> $IBLD_MINIROOT/kernel/drv/sd.conf

echo "Updating $IBLD_MINIROOT/etc/system ..."
#echo "set apix_enable=0" >> $IBLD_MINIROOT/etc/system
echo "set pcplusmp:apic_panic_on_nmi=1" >> $IBLD_MINIROOT/etc/system
echo "set dump_plat_mincpu=0" >> $IBLD_MINIROOT/etc/system
#echo "set platform_type=0" >> $IBLD_MINIROOT/etc/system
