INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
# python-funcsigs
INSTDEPENDS += python-pbr
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
# python-unittest2
INSTDEPENDS += python3-all
INSTDEPENDS += python3-pbr
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-six
# python3-unittest2
