INSTDEPENDS += debhelper
INSTDEPENDS += ant
# default-jdk
#Build-Depends-Indep:
INSTDEPENDS += ant-optional
INSTDEPENDS += ivy
INSTDEPENDS += junit
INSTDEPENDS += libbcel-java
INSTDEPENDS += libcommons-httpclient-java
INSTDEPENDS += libxerces2-java
INSTDEPENDS += maven-repo-helper
