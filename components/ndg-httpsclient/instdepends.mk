INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# openssl
INSTDEPENDS += python-all
INSTDEPENDS += python-openssl
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-all
INSTDEPENDS += python3-openssl
INSTDEPENDS += python3-setuptools
