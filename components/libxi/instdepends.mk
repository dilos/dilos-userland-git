INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += quilt
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += x11proto-input-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += libxfixes-dev
INSTDEPENDS += xmlto
INSTDEPENDS += asciidoc
INSTDEPENDS += pkg-config
INSTDEPENDS += quilt
INSTDEPENDS += xutils-dev
INSTDEPENDS += automake
INSTDEPENDS += libtool
# specs
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += xsltproc
# w3m
