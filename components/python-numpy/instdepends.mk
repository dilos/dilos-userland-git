INSTDEPENDS += debhelper
INSTDEPENDS += cython
INSTDEPENDS += dh-python
# gfortran
#INSTDEPENDS += libblas-dev
#INSTDEPENDS += liblapack-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-nose
INSTDEPENDS += python-tz
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-nose
INSTDEPENDS += python3-tz
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
#Build-Depends-Indep:
INSTDEPENDS += python-docutils
#INSTDEPENDS += python-matplotlib
# python-sphinx
