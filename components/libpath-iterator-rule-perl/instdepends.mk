INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += devscripts
# perl
INSTDEPENDS += dh-buildinfo
INSTDEPENDS += libtext-glob-perl
INSTDEPENDS += libtry-tiny-perl
INSTDEPENDS += libnumber-compare-perl
INSTDEPENDS += libfile-pushd-perl
INSTDEPENDS += libpath-tiny-perl
INSTDEPENDS += libtest-deep-perl
INSTDEPENDS += libtest-filename-perl
#INSTDEPENDS += libcpan-meta-perl
#INSTDEPENDS += libcpan-meta-requirements-perl
