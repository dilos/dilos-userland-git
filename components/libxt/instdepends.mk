INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libx11-dev
INSTDEPENDS += libsm-dev
INSTDEPENDS += libice-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += xutils-dev
INSTDEPENDS += quilt
# for unit tests
INSTDEPENDS += libglib2.0-dev
# specs
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
#
INSTDEPENDS += libuuid-dev
