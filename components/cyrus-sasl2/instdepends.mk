INSTDEPENDS += debhelper
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
# chrpath
# default-libmysqlclient-dev |
INSTDEPENDS += libmysqlclient-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += docbook-to-man
INSTDEPENDS += groff-base
INSTDEPENDS += heimdal-multidev
INSTDEPENDS += krb5-multidev
INSTDEPENDS += libdb-dev
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += libldap2-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpq-dev
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += po-debconf
INSTDEPENDS += quilt
