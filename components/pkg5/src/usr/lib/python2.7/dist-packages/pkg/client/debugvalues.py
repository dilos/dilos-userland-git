#!/usr/bin/python
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

class Singleton(type):
        """Set __metaclass__ to Singleton to create a singleton.
        See http://en.wikipedia.org/wiki/Singleton_pattern """

        def __init__(self, name, bases, dictionary):
                super(Singleton, self).__init__(name, bases, dictionary)
                self.instance = None

        def __call__(self, *args, **kw):
                if self.instance is None:
                        self.instance = super(Singleton, self).__call__(*args,
                            **kw)

                return self.instance


class DebugValues(dict):
        """Singleton dict that returns None if unknown value
        is referenced"""
        __metaclass__ = Singleton

        def __getitem__(self, item):
                """ returns None if not set """
                return self.get(item, None)

        def get_value(self, key):
                return self[key]

        def set_value(self, key, value):
                self[key] = value


DebugValues=DebugValues()
