#!/usr/bin/python
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

IMG_NONE = 0
IMG_ENTIRE = 1
IMG_PARTIAL = 2
IMG_USER = 3

img_type_names = {
    IMG_NONE: "none",
    IMG_ENTIRE: "full",
    IMG_PARTIAL: "partial",
    IMG_USER: "user"
}

