#!/usr/bin/python
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"%Z%%M%	%I%	%E% SMI"

import gzip

class PkgGzipFile(gzip.GzipFile):
        """This is a version of GzipFile that does not include a file
        pathname or timestamp in the gzip header.  This allows us to get
        deterministic gzip files on compression, so that we can reliably
        use a cryptographic hash on the compressed content."""

        def __init__(self, filename=None, mode=None, compresslevel=9,
            fileobj=None):

               gzip.GzipFile.__init__(self, filename, mode, compresslevel,
                    fileobj) 

        #
        # This is a gzip header conforming to RFC1952.  The first two bytes
        # (\037,\213) are the gzip magic number.  The third byte is the
        # compression method (8, deflate).  The fourth byte is the flag byte
        # (0), which indicates that no FNAME, FCOMMENT or other extended data
        # is present.  Bytes 5-8 are the MTIME field, zeroed in this case.
        # Byte 9 is the XFL (Extra Flags) field, set to 2 (compressor used
        # max compression).  The final bit is the OS type, set to 255 (for
        # "unknown").
        magic = "\037\213\010\000\000\000\000\000\002\377"

        def _write_gzip_header(self):
                self.fileobj.write(self.magic)

        @staticmethod
        def test_is_pkggzipfile(path):
                f = open(path, "rb")
                hdrstr = f.read(len(PkgGzipFile.magic))
                f.close()
                return (hdrstr == PkgGzipFile.magic)
