#!/usr/bin/python
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# Copyright (c) 2009, 2015, Oracle and/or its affiliates. All rights reserved.
#

"""
Most if not all of the os_unix methods apply on AIX. The methods
below override the definitions from os_unix
"""

import os
import errno
from os_unix import \
    get_isainfo, get_release, get_platform, get_group_by_name, \
    get_user_by_name, get_name_by_gid, get_name_by_uid, is_admin, get_userid, \
    get_username, rename, remove, link, split_path, get_root, assert_mode, copyfile

def chown(path, owner, group):
        # The "nobody" user on AIX has uid -2, which is an invalid UID on NFS
        # file systems mounted from non-AIX hosts.
        # However, we don't want to fail an install because of this.
        try:
                return os.chown(path, owner, group)
        except EnvironmentError as e:
                if owner == -2 and e.errno == errno.EINVAL:
                        return os.chown(path, -1, group)
                raise



