INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += libgmp-dev
INSTDEPENDS += texinfo
INSTDEPENDS += libunistring-dev
INSTDEPENDS += libgc-dev
INSTDEPENDS += libffi-dev
INSTDEPENDS += pkg-config
