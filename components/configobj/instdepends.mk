INSTDEPENDS += debhelper
INSTDEPENDS += python-all
INSTDEPENDS += python-docutils
INSTDEPENDS += python-epydoc
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
# python-sphinx
INSTDEPENDS += python-unittest2
INSTDEPENDS += python3-all
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-six
