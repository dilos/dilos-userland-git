INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += quilt
# xutils-dev
INSTDEPENDS += pkg-config
# specs:
INSTDEPENDS += xmlto
# xorg-sgml-doctools
# w3m
INSTDEPENDS += xsltproc
# fop
