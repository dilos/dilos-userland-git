INSTDEPENDS += debhelper
INSTDEPENDS += autopoint
INSTDEPENDS += autotools-dev
# bsdmainutils
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += groff
# libbsd-dev
# libkrb5-dev | heimdal-dev
# procps
INSTDEPENDS += texinfo
# texlive-latex-base, texlive-fonts-recommended,
# texlive-latex-recommended
INSTDEPENDS += zlib1g-dev
