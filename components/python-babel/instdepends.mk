INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
# python-sphinx | python3-sphinx,
INSTDEPENDS += python-tz
INSTDEPENDS += python3-all
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-tz
