INSTDEPENDS += debhelper
INSTDEPENDS += libxau-dev
INSTDEPENDS += libxdmcp-dev
INSTDEPENDS += xcb-proto
INSTDEPENDS += libpthread-stubs0-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += xutils-dev
INSTDEPENDS += xsltproc
INSTDEPENDS += check
INSTDEPENDS += python-xcbgen
INSTDEPENDS += libtool
INSTDEPENDS += automake
INSTDEPENDS += python
INSTDEPENDS += dctrl-tools
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
