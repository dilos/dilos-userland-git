INSTDEPENDS += debhelper
INSTDEPENDS += dctrl-tools
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
# dh-systemd
INSTDEPENDS += groff
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libnss3-dev
INSTDEPENDS += libqb-dev
# librdmacm-dev [linux-any]
INSTDEPENDS += libsnmp-dev
# libsystemd-dev [linux-any]
INSTDEPENDS += libxml2-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += zlib1g-dev
#Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += libdevinfo-dev
# libstatgrab is Linux-only until #823899 and #823900 gets fixed:
# libstatgrab-dev [linux-any]
INSTDEPENDS += libstatgrab-dev
# dep from libstatgrab
INSTDEPENDS += libtinfo-dev
