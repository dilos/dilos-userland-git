INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-augeas
INSTDEPENDS += python-configargparse
INSTDEPENDS += python-certbot
INSTDEPENDS += python-mock
INSTDEPENDS += python-openssl
INSTDEPENDS += python-parsedatetime
INSTDEPENDS += python-requests
INSTDEPENDS += python-rfc3339
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
# python-sphinx
# python-sphinx-rtd-theme
INSTDEPENDS += python-tz
INSTDEPENDS += python-zope.component
INSTDEPENDS += python-zope.interface
