INSTDEPENDS += debhelper
#Build-Depends-Indep:
# default-jdk
INSTDEPENDS += libarchive-zip-perl
INSTDEPENDS += markdown
# perl
INSTDEPENDS += libtest-minimumversion-perl
INSTDEPENDS += libtest-perl-critic-perl
INSTDEPENDS += libtest-strict-perl
# perl (>= 5.12) | libtest-simple-perl (>= 0.93)
