INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# pypy
# pypy-setuptools
INSTDEPENDS += python
INSTDEPENDS += python-all
INSTDEPENDS += python-py
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3
INSTDEPENDS += python3-all
INSTDEPENDS += python3-py
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
