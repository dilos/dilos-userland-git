INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += bison
INSTDEPENDS += flex
INSTDEPENDS += intltool
INSTDEPENDS += liborbit2-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libpopt-dev
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += docbook-xml
INSTDEPENDS += autotools-dev
# libglib2.0-doc
