INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libx11-dev
INSTDEPENDS += x11proto-xf86vidmode-dev
# libx11-6
INSTDEPENDS += libxext-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += quilt
INSTDEPENDS += xutils-dev
