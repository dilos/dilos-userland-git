INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-cryptography
INSTDEPENDS += python-dnspython
INSTDEPENDS += python-docutils
INSTDEPENDS += python-mock
INSTDEPENDS += python-ndg-httpsclient
INSTDEPENDS += python-openssl
INSTDEPENDS += python-pyasn1
INSTDEPENDS += python-requests
INSTDEPENDS += python-rfc3339
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
# python-sphinx
# python-sphinx-rtd-theme
# python-sphinxcontrib.programoutput
INSTDEPENDS += python-tz
INSTDEPENDS += python3
INSTDEPENDS += python3-cryptography
INSTDEPENDS += python3-dnspython
INSTDEPENDS += python3-docutils
INSTDEPENDS += python3-mock
INSTDEPENDS += python3-ndg-httpsclient
INSTDEPENDS += python3-openssl
INSTDEPENDS += python3-pyasn1
INSTDEPENDS += python3-requests
INSTDEPENDS += python3-rfc3339
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-tz
