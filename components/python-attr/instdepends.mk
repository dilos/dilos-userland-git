INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# pypy
# pypy-setuptools
INSTDEPENDS += python-all
INSTDEPENDS += python-hypothesis
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
# python-sphinx-rtd-theme
INSTDEPENDS += python-zope.interface
INSTDEPENDS += python3-all
# python3-doc
INSTDEPENDS += python3-hypothesis
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
# python3-sphinx
INSTDEPENDS += python3-zope.interface
