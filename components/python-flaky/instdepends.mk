INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# pypy
# pypy-setuptools
INSTDEPENDS += python-all
INSTDEPENDS += python-genty
INSTDEPENDS += python-mock
INSTDEPENDS += python-nose
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-all
INSTDEPENDS += python3-genty
INSTDEPENDS += python3-mock
INSTDEPENDS += python3-nose
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
