INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += quilt
INSTDEPENDS += pkg-config
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += w3m
