INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
#  qtbase5-dev <!stage1>,
INSTDEPENDS += flex
INSTDEPENDS += bison
INSTDEPENDS += python3
INSTDEPENDS += libxapian-dev
INSTDEPENDS += cmake
# yui-compressor
#Build-Depends-Indep: texlive-fonts-recommended,
#  texlive-generic-recommended,
#  texlive-latex-extra,
#  texlive-latex-recommended,
#  texlive-extra-utils,
#  texlive-font-utils,
#  ghostscript,
#  graphviz,
#  latex-xcolor,
#  rdfind
