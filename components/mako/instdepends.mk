INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
# to rebuild docs:
# python-sphinx, python-changelog, python-sphinx-paramlinks,
# for tests:
# python-pytest, python-markupsafe, python-mock,
# python3-pytest, python3-markupsafe, python3-mock
