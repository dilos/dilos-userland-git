INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += gperf
INSTDEPENDS += guile-2.0-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += pkg-config
# tar
# autogen <cross>
#Build-Depends-Indep:
INSTDEPENDS += texinfo
INSTDEPENDS += texlive
INSTDEPENDS += cm-super-minimal
