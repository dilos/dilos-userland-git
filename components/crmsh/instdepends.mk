INSTDEPENDS += debhelper
INSTDEPENDS += asciidoc
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-python
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += dos2unix
INSTDEPENDS += libxml2-utils
INSTDEPENDS += pkg-config
INSTDEPENDS += python-all
INSTDEPENDS += python-dateutil
INSTDEPENDS += python-pygments
INSTDEPENDS += python-setuptools
INSTDEPENDS += xsltproc
