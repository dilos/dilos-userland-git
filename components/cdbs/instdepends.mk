INSTDEPENDS += debhelper
INSTDEPENDS += dh-buildinfo
INSTDEPENDS += autoconf
INSTDEPENDS += realpath
INSTDEPENDS += fakeroot
INSTDEPENDS += python-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all-dev
INSTDEPENDS += ant
# default-jdk-headless | default-jdk,
INSTDEPENDS += sharutils
INSTDEPENDS += lzma
