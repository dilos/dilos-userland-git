INSTDEPENDS += debhelper
INSTDEPENDS += dc
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += dh-lua
INSTDEPENDS += dh-python
INSTDEPENDS += gem2deb
INSTDEPENDS += libdbi-dev
INSTDEPENDS += libpango1.0-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libxml2-dev
# perl
# python-all-dbg (>= 2.6.6-3~),
INSTDEPENDS += python-all-dev
INSTDEPENDS += tcl-dev
