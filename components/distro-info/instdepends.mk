INSTDEPENDS += debhelper
INSTDEPENDS += distro-info-data
INSTDEPENDS += pylint
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
#INSTDEPENDS += python-unittest2
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
INSTDEPENDS += shunit2
