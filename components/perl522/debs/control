Source: perl
Section: perl
Priority: standard
Maintainer: Niko Tyni <ntyni@debian.org>
Uploaders: Dominic Hargreaves <dom@earth.li>
Standards-Version: 3.9.6
Homepage: http://dev.perl.org/perl5/
Build-Depends: file, cpio, libdb-dev, libgdbm-dev, netbase [!hurd-any],
 procps [!hurd-any], zlib1g-dev | libz-dev, libbz2-dev, dpkg-dev (>= 1.16.0),
 libc6-dev (>= 2.19-9) [s390x]
Vcs-Git: git://anonscm.debian.org/perl/perl.git -b debian-5.22
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=perl/perl.git

Package: perl-base-5.22
#Essential: yes
Priority: required
Architecture: any
Pre-Depends: ${shlibs:Depends}, dpkg (>= 1.16.2)
#Depends: ${shlibs:Depends}
Replaces: perl (<< 1.3.7), perl-modules (<< 1.3.7),
Provides: ${perlapi:Provides},
 libscalar-list-utils-perl,
 libxsloader-perl,
 libsocket-perl,
 libfile-temp-perl,
 libfile-path-perl,
 libio-socket-ip-perl,
Suggests: perl
Description: minimal Perl system
 Perl is a scripting language used in many system scripts and utilities.
 .
 This package provides a Perl interpreter and the small subset of the
 standard run-time library required to perform basic tasks. For a full
 Perl installation, install "perl" (and its dependencies, "perl-modules-5.22"
 and "perl-doc").

Package: perl-doc
Section: doc
Priority: optional
Architecture: all
Depends: perl (>= 1.3.7.1)
Suggests: man-browser, groff-base
Description: Perl documentation
 Perl manual pages, POD documentation and the `perldoc' program.  If you are
 writing Perl programs, you almost certainly need this.

Package: libperl5.22
Section: libs
Priority: optional
Architecture: any
Depends: ${shlibs:Depends}, perl-modules-5.22 (>= ${source:Version})
Replaces: perl-base (<< 1.3.7~), perl (<< 1.3.7~)
Provides: perl-cross-config,
 libdigest-md5-perl,
 libmime-base64-perl,
 libtime-hires-perl,
 libstorable-perl,
 libdigest-sha-perl,
 libsys-syslog-perl,
 libcompress-zlib-perl,
 libcompress-raw-zlib-perl,
 libcompress-raw-bzip2-perl,
 libio-compress-zlib-perl,
 libio-compress-bzip2-perl,
 libio-compress-base-perl,
 libio-compress-perl,
 libthreads-perl,
 libthreads-shared-perl,
 libtime-piece-perl,
 libencode-perl,
#Multi-Arch: same
Description: shared Perl library
 This package contains the shared Perl library, used by applications
 which embed a Perl interpreter.
 .
 It also contains the architecture-dependent parts of the standard
 library (and depends on perl-modules-5.22 which contains the
 architecture-independent parts).

Package: libperl-dev
Section: libdevel
Priority: optional
Architecture: any
Depends: libperl5.22 (= ${binary:Version}),
 libc-dev
Description: Perl library: development files
 Files for developing applications which embed a Perl interpreter.

Package: perl-modules-5.22
Priority: standard
Architecture: all
#Multi-Arch: foreign
Depends: perl-base (>= 1.3.7.1)
Pre-Depends: dpkg (>= 1.16.2)
Recommends: perl (>= 1.3.7.1)
#Replaces: perl-modules
Provides: perl-modules,
 libpod-parser-perl,
 libansicolor-perl,
 libnet-perl,
 libattribute-handlers-perl,
 libi18n-langtags-perl,
 liblocale-maketext-perl,
 libmath-bigint-perl,
 libnet-ping-perl,
 libtest-harness-perl,
 libtest-simple-perl,
 liblocale-codes-perl,
 libmodule-corelist-perl,
 libio-zlib-perl,
 libarchive-tar-perl,
 libextutils-cbuilder-perl,
 libmodule-load-perl,
 liblocale-maketext-simple-perl,
 libparams-check-perl,
 libmodule-load-conditional-perl,
 libversion-perl,
 libpod-simple-perl,
 libextutils-parsexs-perl,
 libpod-escapes-perl,
 libparse-cpan-meta-perl,
 libparent-perl,
 libautodie-perl,
 libthread-queue-perl,
 libfile-spec-perl,
 libtime-local-perl,
 podlators-perl,
 libunicode-collate-perl,
 libcpan-meta-perl,
 libmath-complex-perl,
 libextutils-command-perl,
 libmodule-metadata-perl,
 libjson-pp-perl,
 libperl-ostype-perl,
 libversion-requirements-perl,
 libcpan-meta-yaml-perl,
 libdigest-perl,
 libextutils-install-perl,
 libhttp-tiny-perl,
 libcpan-meta-requirements-perl,
 libexperimental-perl,
 libtest-use-ok-perl,
 libtest-tester-perl,
Description: Core Perl modules
 Architecture independent Perl modules.  These modules are part of Perl and
 required if the `perl' package is installed.
 .
 Note that this package only exists to save archive space and should be
 considered an internal implementation detail of the `perl' package.
 Other packages should not depend on `perl-modules-5.22' directly, they
 should use `perl' (which depends on `perl-modules-5.22') instead.

Package: perl
Priority: standard
Architecture: any
#Multi-Arch: allowed
Pre-Depends: dpkg (>= 1.16.2)
Depends: perl-base (= ${binary:Version}), perl-modules-5.22 (>= ${source:Version}), libperl5.22 (= ${binary:Version}), ${shlibs:Depends}
#Replaces: perl-modules (<< 5.22.0~)
Recommends: netbase, rename
Suggests: perl-doc, libterm-readline-gnu-perl | libterm-readline-perl-perl,
 gmake
Description: Larry Wall's Practical Extraction and Report Language
 Perl is a highly capable, feature-rich programming language with over
 20 years of development. Perl 5 runs on over 100 platforms from
 portables to mainframes. Perl is suitable for both rapid prototyping
 and large scale development projects.
 .
 Perl 5 supports many programming styles, including procedural,
 functional, and object-oriented. In addition to this, it is supported
 by an ever-growing collection of reusable modules which accelerate
 development. Some of these modules include Web frameworks, database
 integration, networking protocols, and encryption. Perl provides
 interfaces to C and C++ for custom extension development.

Package: perl-base
Depends: perl-base-5.22 (= ${binary:Version})
Priority: optional
Section: perl
Architecture: all
Description: minimal Perl system
 Empty package to facilitate upgrades, can be safely removed.
