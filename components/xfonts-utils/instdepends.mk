INSTDEPENDS += debhelper
INSTDEPENDS += pkg-config
INSTDEPENDS += quilt
INSTDEPENDS += autotools-dev
#INSTDEPENDS += libxfont1-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += libfreetype6-dev
#INSTDEPENDS += libfontenc-dev
INSTDEPENDS += xutils-dev
