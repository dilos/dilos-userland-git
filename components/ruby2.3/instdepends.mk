INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += chrpath
INSTDEPENDS += dh-autoreconf
#INSTDEPENDS += file
INSTDEPENDS += libffi-dev
INSTDEPENDS += libgdbm-dev
INSTDEPENDS += libgmp-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += libreadline6-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libyaml-dev
# netbase
# procps
# ruby | ruby-interpreter |
#INSTDEPENDS += ruby1.8
INSTDEPENDS += ruby2.1
#INSTDEPENDS += rubygems-integration
INSTDEPENDS += tcl8.6-dev
INSTDEPENDS += tk8.6-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += dtrace
