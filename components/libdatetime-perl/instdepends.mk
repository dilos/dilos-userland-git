INSTDEPENDS += debhelper
INSTDEPENDS += libcpan-meta-check-perl
INSTDEPENDS += libdatetime-locale-perl
INSTDEPENDS += libdatetime-timezone-perl
INSTDEPENDS += libdist-checkconflicts-perl
INSTDEPENDS += libnamespace-autoclean-perl
INSTDEPENDS += libparams-validationcompiler-perl
INSTDEPENDS += libspecio-perl
INSTDEPENDS += libtest-cleannamespaces-perl
INSTDEPENDS += libtest-fatal-perl
INSTDEPENDS += libtest-warnings-perl
INSTDEPENDS += libtry-tiny-perl
# perl
# perl (>= 5.15.7) | libcpan-meta-requirements-perl
