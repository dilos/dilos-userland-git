#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright (c) 2011, 2012, Oracle and/or its affiliates. All rights reserved.
# Copyright 2016 Igor Kozhukhov <ikozhukhov@gmail.com>
#
#CONFIGURE_DEFAULT_LDFLAGS = no
#CONFIGURE_DEFAULT_CPPFLAGS = no
include ../../make-rules/shared-macros.mk

COMPONENT_NAME=		screen
COMPONENT_VERSION=	4.3.1
COMPONENT_PROJECT_URL=	http://www.gnu.org/software/screen/
COMPONENT_SRC=		$(COMPONENT_NAME)-$(COMPONENT_VERSION)
COMPONENT_ARCHIVE=	$(COMPONENT_SRC).tar.gz
COMPONENT_ARCHIVE_HASH=	\
	sha256:fa4049f8aee283de62e283d427f2cfd35d6c369b40f7f45f947dbfd915699d63
COMPONENT_ARCHIVE_URL=	http://ftp.gnu.org/gnu/screen/$(COMPONENT_ARCHIVE)

include ../../make-rules/prep.mk
include ../../make-rules/configure.mk
include $(WS_TOP)/make-rules/deb2.mk
include $(WS_TOP)/make-rules/64bit.mk

COMPONENT_PREP_ACTION = \
	( cd $(@D) ; autoreconf )

# Assert the use of fifos instead of sockets
COMPONENT_POST_CONFIGURE_ACTION = \
	(cd $(@D) ; \
	ggrep -q "define.*NAMEDPIPE.*1" config.h || echo "\#define NAMEDPIPE 1" >> config.h)

CONFIGURE_OPTIONS +=	--enable-colors256
CONFIGURE_OPTIONS +=	--with-sys-screenrc=/etc/screenrc
CONFIGURE_OPTIONS +=	--disable-socket-dir
CONFIGURE_OPTIONS +=	--infodir=$(CONFIGURE_INFODIR)
CONFIGURE_OPTIONS +=	--enable-pam
CONFIGURE_OPTIONS +=	--enable-telnet
CONFIGURE_OPTIONS +=	--enable-use-locale
CONFIGURE_OPTIONS +=	--enable-rxvt_osc

build:		$(BUILD_64)

install:	$(INSTALL_64)

test:		$(NO_TESTS)
