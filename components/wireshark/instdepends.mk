INSTDEPENDS += debhelper
INSTDEPENDS += libgtk-3-dev
# enable Qt 5 related dependencies for Qt 5-enabled build
#INSTDEPENDS += qtbase5-dev
#INSTDEPENDS += qtbase5-dev-tools
#INSTDEPENDS += qttools5-dev
#INSTDEPENDS += qttools5-dev-tools
#INSTDEPENDS += qtmultimedia5-dev
#INSTDEPENDS += libqt5svg5-dev
# enable Qt 4 related dependencies for Qt 4-enabled build
# qt4-qmake, libqt4-dev,
INSTDEPENDS += libpcap0.8-dev
INSTDEPENDS += flex
INSTDEPENDS += libz-dev
INSTDEPENDS += po-debconf
INSTDEPENDS += libtool
INSTDEPENDS += python
INSTDEPENDS += python-ply
# libc-ares-dev
INSTDEPENDS += xsltproc
INSTDEPENDS += dh-python
INSTDEPENDS += docbook-xsl
INSTDEPENDS += docbook-xml
INSTDEPENDS += libxml2-utils
INSTDEPENDS += libpcre3-dev
# (>= 2.17) [linux-any]
#INSTDEPENDS += libcap-dev
INSTDEPENDS += lsb-release
INSTDEPENDS += bison
# enable versioned libgnutls28-dev below to have GPLv2+ compatible GMP
INSTDEPENDS += libgnutls28-dev
# enable backports-compatible libgnutls-dev
# libgnutls-dev,
INSTDEPENDS += libgcrypt-dev
INSTDEPENDS += portaudio19-dev
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += liblua5.2-dev
INSTDEPENDS += libsmi2-dev
INSTDEPENDS += libgeoip-dev
INSTDEPENDS += dpkg-dev
INSTDEPENDS += imagemagick
INSTDEPENDS += xdg-utils
# libnl-genl-3-dev [linux-any]
# libnl-route-3-dev [linux-any]
INSTDEPENDS += asciidoc
INSTDEPENDS += cmake
INSTDEPENDS += w3m
INSTDEPENDS += libsbc-dev
INSTDEPENDS += libnghttp2-dev
INSTDEPENDS += libssh-gcrypt-dev
