INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libpango1.0-dev
INSTDEPENDS += libgdk-pixbuf2.0-dev
INSTDEPENDS += libgtk-3-dev
INSTDEPENDS += libgsf-1-dev
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += libcroco3-dev
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += gobject-introspection
INSTDEPENDS += libgirepository1.0-dev
# libglib2.0-doc
# libcairo2-doc
INSTDEPENDS += valac
