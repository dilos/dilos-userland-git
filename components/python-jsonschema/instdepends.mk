INSTDEPENDS += debhelper
INSTDEPENDS += autopkgtest
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-vcversioner
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-vcversioner
#Build-Depends-Indep:
INSTDEPENDS += python-functools32
INSTDEPENDS += python-mock
INSTDEPENDS += python-nose
INSTDEPENDS += python-pytest
INSTDEPENDS += python3-mock
INSTDEPENDS += python3-nose
INSTDEPENDS += python3-pytest
