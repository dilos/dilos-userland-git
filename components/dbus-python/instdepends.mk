INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
INSTDEPENDS += autoconf-archive
INSTDEPENDS += automake
INSTDEPENDS += dbus
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-python
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libdbus-glib-1-dev
# python-all-dbg (>= 2.6.6-3~),
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-gi
# python3-all-dbg,
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-gi
INSTDEPENDS += xmlto
#Build-Depends-Indep:
INSTDEPENDS += python-docutils
INSTDEPENDS += python-epydoc
