INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += libffi-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-py
INSTDEPENDS += python-pycparser
INSTDEPENDS += python-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-py
INSTDEPENDS += python3-pycparser
INSTDEPENDS += python3-pytest
INSTDEPENDS += python3-setuptools
# virtualenv | python-virtualenv (<< 1.11.6)
