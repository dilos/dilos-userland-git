INSTDEPENDS += debhelper
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libidl-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += cdbs
INSTDEPENDS += docbook-xsl
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
