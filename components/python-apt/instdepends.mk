INSTDEPENDS += debhelper
# apt
# apt-utils
INSTDEPENDS += dh-python
# fakeroot
INSTDEPENDS += libapt-pkg-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python-distutils-extra
# python-sphinx
INSTDEPENDS += pep8
INSTDEPENDS += pyflakes
