INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
#    python3-sphinx
#    python3-sphinx-rtd-theme
#    python3-sphinxcontrib.spelling
INSTDEPENDS += python-docutils
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-all-dev
INSTDEPENDS += python
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3
