INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += quilt
INSTDEPENDS += pkg-config
INSTDEPENDS += libdrm-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += x11proto-gl-dev
INSTDEPENDS += libxxf86vm-dev
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libgcrypt20-dev
# [linux-amd64 linux-i386 linux-x32],
INSTDEPENDS += libxfixes-dev
INSTDEPENDS += libxdamage-dev
INSTDEPENDS += libxext-dev
#INSTDEPENDS += libva-dev - linux
# libva-dev (>= 1.6.0) [linux-any kfreebsd-any],
#INSTDEPENDS += libvdpau-dev
# libvdpau-dev (>= 1.1.1) [linux-any kfreebsd-any],
# libvulkan-dev [linux-amd64 linux-i386 linux-x32],
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += x11proto-dri2-dev
INSTDEPENDS += x11proto-dri3-dev
INSTDEPENDS += x11proto-present-dev
# linux-libc-dev (>= 2.6.31) [linux-any],
INSTDEPENDS += libx11-xcb-dev
INSTDEPENDS += libxcb-dri2-0-dev
INSTDEPENDS += libxcb-glx0-dev
INSTDEPENDS += libxcb-xfixes0-dev
INSTDEPENDS += libxcb-dri3-dev
INSTDEPENDS += libxcb-present-dev
INSTDEPENDS += libxcb-randr0-dev
INSTDEPENDS += libxcb-sync-dev
# libxshmfence-dev - linux-any
INSTDEPENDS += libxshmfence-dev
INSTDEPENDS += python
INSTDEPENDS += python-mako
INSTDEPENDS += flex
INSTDEPENDS += bison
# llvm-3.8-dev (>= 1:3.8) [amd64 i386 kfreebsd-amd64 kfreebsd-i386 arm64 armhf ppc64el],
# libelf-dev [amd64 i386 kfreebsd-amd64 kfreebsd-i386 arm64 armhf ppc64el],
INSTDEPENDS += libelf-dev
# libwayland-dev (>= 1.2.0) [linux-any],
# libclang-3.8-dev (>= 1:3.8) [amd64 i386 arm64 armhf],
# libclc-dev (>= 0.2.0+git20150813) [amd64 i386 arm64 armhf],
# libclc-dev - need clang libs
INSTDEPENDS += nettle-dev
