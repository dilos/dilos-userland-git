INSTDEPENDS += debhelper
INSTDEPENDS += cluster-glue-dev
# debian/check_header_deps needs:
INSTDEPENDS += dctrl-tools
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
# dh-systemd
# resource agent man pages are generated via:
INSTDEPENDS += docbook-xsl
INSTDEPENDS += help2man
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libcfg-dev
INSTDEPENDS += libcmap-dev
INSTDEPENDS += libcpg-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libesmtp-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libltdl-dev
INSTDEPENDS += libncurses5-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libqb-dev
INSTDEPENDS += libquorum-dev
# net-snmp-config --agent-libs contains -lsensors in sid on 2015-09-15
INSTDEPENDS += libsensors4-dev
INSTDEPENDS += libsnmp-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxml2-utils
INSTDEPENDS += libxslt1-dev
INSTDEPENDS += pkg-config
# systemd [linux-any]
# uuid-dev,
INSTDEPENDS += libuuid-dev
# detecting docbook-xsl needs:
INSTDEPENDS += xsltproc
#Build-Depends-Indep:
INSTDEPENDS += asciidoc
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += inkscape
INSTDEPENDS += publican
