INSTDEPENDS += cdbs
INSTDEPENDS += autotools-dev
INSTDEPENDS += devscripts
INSTDEPENDS += debhelper
INSTDEPENDS += dh-buildinfo
INSTDEPENDS += libdb-dev
INSTDEPENDS += libwrap0-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
# libcups2-dev
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += libltdl3-dev
INSTDEPENDS += libgcrypt11-dev
INSTDEPENDS += libcrack2-dev
INSTDEPENDS += libavahi-client-dev
INSTDEPENDS += libldap2-dev
# libacl1-dev
INSTDEPENDS += d-shlibs
INSTDEPENDS += dh-exec
