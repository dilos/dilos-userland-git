INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += cdbs
INSTDEPENDS += libtool
# perl
INSTDEPENDS += libraptor2-dev
INSTDEPENDS += librasqal3-dev
INSTDEPENDS += libdb-dev
INSTDEPENDS += libmysqlclient-dev
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libpq-dev
INSTDEPENDS += libltdl-dev
INSTDEPENDS += unixodbc-dev
INSTDEPENDS += gtk-doc-tools
