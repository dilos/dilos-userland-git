INSTDEPENDS += debhelper
INSTDEPENDS += libpam-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libsasl2-dev
# libselinux1-dev [linux-any]
INSTDEPENDS += autoconf
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += flex
# libaudit-dev [linux-any]
INSTDEPENDS += mandoc
#
INSTDEPENDS += libbsm-dev
