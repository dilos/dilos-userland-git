INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += python3.5
INSTDEPENDS += lsb-release
INSTDEPENDS += python3-minimal
INSTDEPENDS += python3.5-minimal
INSTDEPENDS += python3-docutils
INSTDEPENDS += debiandoc-sgml
