INSTDEPENDS += debhelper
# bash-completion
INSTDEPENDS += cmake
INSTDEPENDS += dh-python
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += libart-2.0-dev
INSTDEPENDS += libaspell-dev
INSTDEPENDS += libboost-dev
INSTDEPENDS += libcdr-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libdbus-glib-1-dev
INSTDEPENDS += libgc-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libgsl-dev
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += libgtkmm-2.4-dev
INSTDEPENDS += libgtkspell-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += libmagick++-dev
INSTDEPENDS += libpango1.0-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libpoppler-glib-dev
INSTDEPENDS += libpoppler-private-dev
INSTDEPENDS += libpopt-dev
INSTDEPENDS += libpotrace-dev
INSTDEPENDS += librevenge-dev
INSTDEPENDS += libsigc++-2.0-dev
INSTDEPENDS += libtool
INSTDEPENDS += libvisio-dev
INSTDEPENDS += libwpg-dev
INSTDEPENDS += libxml-parser-perl
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxslt1-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += python-dev
INSTDEPENDS += python-lxml
INSTDEPENDS += zlib1g-dev
