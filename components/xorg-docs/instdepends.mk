INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += quilt
INSTDEPENDS += pkg-config
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += docbook-xml
INSTDEPENDS += automake
INSTDEPENDS += autoconf
INSTDEPENDS += xutils-dev
INSTDEPENDS += xmlto
# fop
INSTDEPENDS += w3m
