INSTDEPENDS += debhelper
#Build-Depends-Indep:
# perl-modules
INSTDEPENDS += libmodule-build-perl
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += xsltproc
INSTDEPENDS += libterm-readkey-perl
# unit tests
INSTDEPENDS += gettext
#INSTDEPENDS += libunicode-linebreak-perl
#   SGML
INSTDEPENDS += opensp
INSTDEPENDS += docbook
INSTDEPENDS += libsgmls-perl
#   TeX
# texlive-binaries
