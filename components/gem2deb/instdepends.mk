INSTDEPENDS += debhelper
# dctrl-tools
# devscripts <!nocheck>,
INSTDEPENDS += dpkg-dev
# lintian
#INSTDEPENDS += python3-debian
# rake
INSTDEPENDS += ruby
#INSTDEPENDS += ruby-all-dev
# ruby-mocha
# ruby-rspec
# ruby-setup
# ruby-shoulda-context
# ruby-test-unit
