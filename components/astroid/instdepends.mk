INSTDEPENDS += debhelper
INSTDEPENDS += python
INSTDEPENDS += python3
INSTDEPENDS += dh-python
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python-nose
INSTDEPENDS += python3-nose
# python-wrapt
# python3-wrapt
# python-lazy-object-proxy
# python3-lazy-object-proxy
