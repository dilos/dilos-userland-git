INSTDEPENDS += debhelper
INSTDEPENDS += python-minimal
INSTDEPENDS += pkg-config
INSTDEPENDS += cmake
INSTDEPENDS += libz-dev
INSTDEPENDS += libcurl4-gnutls-dev
INSTDEPENDS += libssh2-1-dev
INSTDEPENDS += libhttp-parser-dev
