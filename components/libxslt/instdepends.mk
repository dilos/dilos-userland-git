INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-python
INSTDEPENDS += libgcrypt11-dev
INSTDEPENDS += libxml2-dev
# perl
INSTDEPENDS += python-all-dev
