INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += libxmu-dev
INSTDEPENDS += libxpm-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += quilt
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
INSTDEPENDS += dh-autoreconf
# specs:
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
# w3m
