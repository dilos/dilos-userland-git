INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += chrpath
# coreutils
INSTDEPENDS += dh-autoreconf
# file
INSTDEPENDS += libffi-dev
INSTDEPENDS += libgdbm-dev
INSTDEPENDS += libgmp-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += libreadline6-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libyaml-dev
# openssl
# procps
# ruby | ruby-interpreter |
INSTDEPENDS += ruby1.8
# rubygems-integration (>= 1.6)
INSTDEPENDS += tcl8.6-dev
INSTDEPENDS += tk8.6-dev
INSTDEPENDS += zlib1g-dev
