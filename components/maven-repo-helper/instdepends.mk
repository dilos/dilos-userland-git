INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
#INSTDEPENDS += default-jdk
INSTDEPENDS += ant
INSTDEPENDS += help2man
INSTDEPENDS += python-docutils
#Build-Depends-Indep:
INSTDEPENDS += ant-optional
INSTDEPENDS += libstax-java
INSTDEPENDS += junit4
INSTDEPENDS += libxmlunit-java
INSTDEPENDS += libcommons-io-java
