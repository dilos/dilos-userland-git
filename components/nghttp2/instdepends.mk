INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libc-ares-dev
INSTDEPENDS += libcunit1-dev
INSTDEPENDS += libev-dev
INSTDEPENDS += libevent-dev
INSTDEPENDS += libjansson-dev
INSTDEPENDS += libjemalloc-dev
INSTDEPENDS += libspdylay-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += zlib1g-dev
# doc
INSTDEPENDS += python-sphinx
