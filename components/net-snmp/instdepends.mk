#INSTDEPENDS += libpkcs11-dev
#INSTDEPENDS += librpcsvc-dev

INSTDEPENDS += debhelper
INSTDEPENDS += libtool
INSTDEPENDS += libwrap0-dev
INSTDEPENDS += libssl1.0-dev
# | libssl-dev (<< 1.1)
# perl
INSTDEPENDS += libperl-dev
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python2.7-dev
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
# debianutils
INSTDEPENDS += dh-autoreconf
# dh-systemd
# bash (>=2.05)
# findutils
# procps
# pkg-config [kfreebsd-i386 kfreebsd-amd64]
# libbsd-dev [kfreebsd-i386 kfreebsd-amd64]
# libkvm-dev [kfreebsd-i386 kfreebsd-amd64]
# libsensors4-dev [!hurd-i386 !kfreebsd-i386 !kfreebsd-amd64]
# default-libmysqlclient-dev
# libpci-dev
INSTDEPENDS += dh-python
