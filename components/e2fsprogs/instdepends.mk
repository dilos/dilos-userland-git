INSTDEPENDS += dh-autoreconf

INSTDEPENDS += debhelper
INSTDEPENDS += gettext
INSTDEPENDS += texinfo
INSTDEPENDS += pkg-config
# gcc-multilib [mips mipsel]
# libfuse-dev [linux-any kfreebsd-any]
# libattr1-dev
# libblkid-dev
INSTDEPENDS += libuuid-dev
# m4
