INSTDEPENDS += debhelper
INSTDEPENDS += automake
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += dh-python
INSTDEPENDS += gnupg-agent
INSTDEPENDS += gnupg
INSTDEPENDS += gpgsm
INSTDEPENDS += libassuan-dev
INSTDEPENDS += libgpg-error-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all-dev
# qtbase5-dev
# scdaemon
INSTDEPENDS += swig
INSTDEPENDS += texinfo
#Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
