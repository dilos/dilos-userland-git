INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxfixes-dev
INSTDEPENDS += x11proto-composite-dev
INSTDEPENDS += pkg-config
# xmlto
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
