INSTDEPENDS += debhelper
# emacs25 | emacs24 | emacs-snapshot
INSTDEPENDS += emacs24
INSTDEPENDS += eperl
INSTDEPENDS += ghostscript
INSTDEPENDS += po-debconf
INSTDEPENDS += tex-common
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += texinfo
