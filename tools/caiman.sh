#!/bin/bash

IPS2DEB="/myshare/builds/dilos-userland/dilos-userland-review/tools/ips2deb.pl"

$IPS2DEB --mfe mog \
--mfg /myshare/builds/dilos-caiman/dilos-caiman/usr/src/pkg/packages.i386 \
-d /myshare/builds/dilos-caiman/dilos-caiman/proto/root_i386 \
--pv 1.0.2.3 \
-o .

DIRS="install-distribution-constructor
install-installadm
install-js2ai
system-install
system-install-auto-install
system-install-auto-install-auto-install-common
system-install-configuration
system-install-gui-install
system-install-media-internal
system-install-tests
system-install-text-install
system-library-install
"

CWD=`pwd`

for DIR in $DIRS
do
    cd $DIR
    echo `pwd`
    eval /myshare/builds/bld
    cd ..
done
