#!/usr/bin/perl

use Getopt::Long;
use strict;

my $tmpl = {};

{

    $$tmpl{'MAINTAINER'} = 'Igor Kozhukhov <igor@dilos.org>';
    $$tmpl{'PKGNAME'} = "dummy";
    $$tmpl{'PKGVER'} = "1.2.3.4";

    my $result = GetOptions ( 	"pv|pversion|package_version=s" => \$$tmpl{'PKGVER'},
				"maintainer=s" => \$$tmpl{'MAINTAINER'});

    my $templates = &initTemplates();
    my $str = fillTemplate($$templates{'changelog'});
    print "$str\n";
}

#------------------------------
# fillTemplate
#------------------------------
sub fillTemplate
{
#    my ( $file ) = @_;
    my ( $template ) = @_;
    
    my $str = '';

    $str = $template;

    my $curDate = qx(date "+%a, %d %h %Y %H:%M:%S %z");
    chomp($curDate);
    $$tmpl{'DATE'} = $curDate;

    $str =~ s/%%PKGNAME%%/$$tmpl{'PKGNAME'}/g;
    $str =~ s/%%PKGVER%%/$$tmpl{'PKGVER'}/g;
    $str =~ s/%%MAINTAINER%%/$$tmpl{'MAINTAINER'}/g;
    $str =~ s/%%DATE%%/$$tmpl{'DATE'}/g;

    return $str;
}

#------------------------------
# initTemplates()
#------------------------------
sub initTemplates
{
    my $initTemplate = {};

# changelog
$$initTemplate{'changelog'} = '%%PKGNAME%% (%%PKGVER%%) unstable; urgency=low

  * build for dilos

 -- %%MAINTAINER%%  %%DATE%%
';

    return $initTemplate;
}
