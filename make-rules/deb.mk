#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright (c) 2012-2013, Igor Kozhukhov <ikozhukhov@gmail.com>.  All rights reserved.
#

# DEB makefile

-include ${HOME}/debs_conf.mk
BUILD_VERSION := $(BUILD_NUM)
#-include component.ver
-include instdepends.mk

CLEAN_PATHS += $(SOURCE_DIR)/.prep
CLEAN_PATHS += $(COMPONENT_DIR)/.deb_prep
CLEAN_PATHS += $(COMPONENT_DIR)/*.deb
CLEAN_PATHS += $(COMPONENT_DIR)/*.changes
CLEAN_PATHS += $(BUILD_DIR)/debs

CLOBBER_PATHS += $(COMPONENT_DIR)/.deb_prep

APT_REPO ?= 		/myshare/repo/dilos
APT_DISTRIBUTION ?= 	du-unstable

DEB_MAINTAINER ?= Igor Kozhukhov <igor@dilos.org>
DEB_ARCHITECTURE ?= solaris-$(MACH)

DEBPKGS ?= $(CANONICAL_MANIFESTS:%.p5m=$(BUILD_DIR)/%.deb_stamp)
APTPKGS ?= $(CANONICAL_MANIFESTS:%.p5m=$(BUILD_DIR)/%.apt_stamp)

IPS2DEB = $(WS_TOOLS)/ips2deb.pl
REPREPRO = /usr/bin/reprepro

DEB_PKG_NAME = $(shell cat $< | grep "set name=pkg.fmri" | sed -e 's/.*pkg:\/\/du\.dilos\.org-localizable\///' | sed -e 's/.*pkg:\///' | sed -e 's/\@.*//' | sed -e 's/[\/_]/-/g')

DEB_BUILD_FLAGS ?= -d -b -uc

BUILD_DEB = \
	cd $(BUILD_DIR)/debs/$(DEB_PKG_NAME) ; \
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	/usr/bin/dpkg-buildpackage $(DEB_BUILD_FLAGS) 2> build.log 1>&2 ; \
	if [ -d $(BUILD_DIR)/debs/$(DEB_PKG_NAME)-dev ]; then \
	cd $(BUILD_DIR)/debs/$(DEB_PKG_NAME)-dev ; \
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	/usr/bin/dpkg-buildpackage $(DEB_BUILD_FLAGS) 2> build.log 1>&2 ; \
	fi

MFEXT ?= 	mangled
PKGTYPE = 	userland

INSTALLED_BUILD_DEPENDS = $(INSTDEPENDS:%=/var/lib/dpkg/info/%.md5sums)

include $(WS_TOP)/make-rules/deb_bld_deps.mk

INSTALLED_INIT_BUILD_DEPENDS_ALL = $(INSTALLED_INIT_BUILD_DEPENDS:%=/var/lib/dpkg/info/%.md5sums)

COMPONENT_PRE_DEB_ACTION = \
	($(RM) $(WS_DEBS)/$(DEB_PKG_NAME)_* $(WS_DEBS)/$(DEB_PKG_NAME)-dev_* )

COMPONENT_POST_DEB_ACTION = \
	( if [ ! -f component.ver ]; then \
	    echo "BUILD_NUM := $(BUILD_NUM)" > component.ver; \
	fi )

#COMPONENT_PRE_DEB_ACTION = \
#	( if [ -f component.ver ]; then \
#	    RES=`/usr/bin/dpkg --compare-versions $(BUILD_VERSION) lt $(BUILD_NUM)`; \
#	    if [ "$$RES" != "0" ]; then \
#		echo ""; \
#		echo "Please update version $(BUILD_NUM)"; \
#		echo ""; \
#		exit 1; \
#	    fi; \
#	fi )
	
BUILD_COMPONENTS_DEPENDS.32 = $(BLDDEPENDS:%=$(WS_COMPONENTS)/%/build/$(MACH32)/.installed)

BUILD_COMPONENTS_DEPENDS.64 = $(BLDDEPENDS:%=$(WS_COMPONENTS)/%/build/$(MACH64)/.installed)

BUILD_COMPONENTS_CLEAN = $(BLDDEPENDS:%=$(MAKE) -C $(WS_COMPONENTS)/% clean; )

BUILD_COMPONENTS_DEBCLOBBER = $(BLDDEPENDS:%=$(MAKE) -C $(WS_COMPONENTS)/% deb-clobber; )

.DEFAULT:	deb

apt:	deb $(APTPKGS)

deb:		$(BUILD_DIR) build install $(BUILD_DIR)/debs $(DEBPKGS)

debprep:	$(COMPONENT_DIR)/.deb_prep

debunprep:
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	apt-get remove -y $(INSTALLED_INIT_BUILD_DEPENDS) ; \
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	apt-get autoremove -y

DEB_PREP ?= yes

ifeq ($(DEB_PREP),yes)
$(SOURCE_DIR)/.prep:	$(COMPONENT_DIR)/.deb_prep

$(CONFIGURE_32):	$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.32)

$(CONFIGURE_64):	$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.64)

$(BUILD_32):		$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.32)

$(BUILD_64):		$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.64)
endif

$(COMPONENT_DIR)/.deb_prep:
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	sudo apt-get install -y $(INSTALLED_INIT_BUILD_DEPENDS) $(INSTDEPENDS)
	$(TOUCH) $@

apt-export:
	@(while true; do \
	    if [ ! -f $(APT_REPO)/db/lockfile ]; then \
		break; \
	    fi; \
	    sleep 5; \
	done)
	(cd $(APT_REPO) && $(REPREPRO) -b . export $(APT_DISTRIBUTION))

$(BUILD_DIR)/%.deb_stamp: $(MANIFEST_BASE)-%.mangled
	( $(IPS2DEB) --noverbose \
	     -p $< \
	     $(PKG_PROTO_DIRS:%=-d %) \
	     -o "$(BUILD_DIR)/debs" \
	     --pv "$(DEB_VERSION)" \
	     --cv "$(COMPONENT_VERSION)" \
	     --maintainer "$(DEB_MAINTAINER)" \
	     --architecture "$(DEB_ARCHITECTURE)" \
	     --category "$(PKGTYPE)" )
	-$(COMPONENT_PRE_DEB_ACTION)
	($(BUILD_DEB) && \
	    $(MV) $(BUILD_DIR)/debs/*.deb $(BUILD_DIR)/debs/*.changes $(WS_DEBS)/)
	-$(COMPONENT_POST_DEB_ACTION)
	touch $@

$(BUILD_DIR)/%.apt_stamp: $(MANIFEST_BASE)-%.mangled
	@(while true; do \
	    if [ ! -f $(APT_REPO)/db/lockfile ]; then \
		break; \
	    fi; \
	    sleep 5; \
	done)
	( cd $(APT_REPO) && \
	    ( $(REPREPRO) -b . \
	    --export=never include $(APT_DISTRIBUTION) \
	    $(WS_DEBS)/$(DEB_PKG_NAME)_*.changes; \
	    if [ -f $(WS_DEBS)/$(DEB_PKG_NAME)-dev_*.changes ]; then \
		$(REPREPRO) -b . \
		--export=never include $(APT_DISTRIBUTION) \
		$(WS_DEBS)/$(DEB_PKG_NAME)-dev_*.changes ; \
	    fi ) )
	touch $@

$(BUILD_DIR)/debs:
	$(MKDIR) $@

deb-clean-ver:
	$(RM) component.ver

/var/lib/dpkg/info/%.md5sums:
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	sudo apt-get install -y $(INSTALLED_INIT_BUILD_DEPENDS) $(INSTDEPENDS)
#	pkgname=`echo $(@F) | sed -e 's;\.md5sums;;'`; \
#	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
#	sudo apt-get install -y $$pkgname

$(WS_COMPONENTS)/%/build/prototype:
	INSTDEPTARGET=`dirname $(@D)`; \
	echo $$INSTDEPTARGET ; \
	$(MAKE) -C $$INSTDEPTARGET JOBS=$(JOBS) install

ifneq ($(BLDDEPENDS),)
$(WS_COMPONENTS)/%/build/$(MACH32)/.installed:
	BLDDEPTARGET=`dirname $(@D)`; \
	BLDDEPTARGET=`dirname $$BLDDEPTARGET`; \
	$(MAKE) -C $$BLDDEPTARGET JOBS=$(JOBS) install

$(WS_COMPONENTS)/%/build/$(MACH64)/.installed:
	BLDDEPTARGET=`dirname $(@D)`; \
	BLDDEPTARGET=`dirname $$target`; \
	$(MAKE) -C $$BLDDEPTARGET JOBS=$(JOBS) install
endif

debinstdep:
	@echo $(INSTDEPENDS)

debuninstdep:
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	apt-get remove -y $(INSTDEPENDS) ; \
	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
	apt-get autoremove -y

debclean:
	$(RM) $(DEBPKGS) $(APTPKGS)
	$(RM) $(COMPONENT_DIR)/*.deb
	$(RM) $(COMPONENT_DIR)/*.changes
	$(RM) $(COMPONENT_DIR)/*.deb_prep
	$(RM) $(BUILD_DIR)/*.mangled
	$(RM) $(BUILD_DIR)/*.mogrified
	$(RM) $(BUILD_DIR)/*.published
	$(RM) $(BUILD_DIR)/*.depend*
	$(RM) -r $(BUILD_DIR)/debs

deb-clobber:	deb-uninstdep clobber
	$(BUILD_COMPONENTS_DEBCLOBBER)

.PHONY: deb debprep debclean debclobber debuninstdep apt apt-export
