#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2015 Igor Kozhukhov <ikozhukhov@gmail.com>.
#

# DEB makefile

-include ${HOME}/debs_conf.mk
-include instdepends.mk

include $(WS_TOP)/make-rules/deb_bld_deps.mk

#DEB_BUILD_FLAGS ?= -d -b -uc -us -j$(BUILD_JOBS)
DEB_BUILD_FLAGS ?= -d -b -uc -us

#export LC_ALL=C
export PATH=/usr/bin:/usr/sbin:/sbin

.DEFAULT:		deb

tst:
	env

deb: $(BUILD_DIR)/.debs2

prep: $(COMPONENT_DIR)/.deb_prep

debinstdeps:
	@echo $(INSTDEPENDS)

debuninstdeps:
	LC_ALL=C DEBIAN_FRONTEND=noninteractive sudo -E  apt-get remove -y $(INSTDEPENDS) ; \
	LC_ALL=C DEBIAN_FRONTEND=noninteractive sudo -E  apt-get autoremove -y
	$(RM) .deb_prep

debclobber: debuninstdeps clean

debgenlog:
	$(WS_TOP)/tools/deb_gen_log.pl

$(COMPONENT_DIR)/.deb_prep:
	LC_ALL=C DEBIAN_FRONTEND=noninteractive sudo -E apt-get install -y \
		$(INSTALLED_INIT_BUILD_DEPENDS) $(INSTDEPENDS)
	$(TOUCH) $@

$(BUILD_DIR)/.prep: $(BUILD_DIR) $(COMPONENT_DIR)/.deb_prep
	cd $(@D); \
	    apt-get source $(COMPONENT_NAME)
	$(TOUCH) $@

$(BUILD_DIR)/.debs2: $(BUILD_DIR)/.prep $(WS_DEBS)
	cd $(BUILD_DIR)/$(COMPONENT_NAME)*; \
	    DEB_BUILD_OPTIONS="parallel=$(JOBS) nocheck" \
		/usr/bin/dpkg-buildpackage $(DEB_BUILD_FLAGS)
	-$(CP) $(BUILD_DIR)/$(COMPONENT_NAME)*/../*.deb $(BUILD_DIR)/$(COMPONENT_NAME)*/../*.changes $(WS_DEBS)/
	-$(COMPONENT_POST_DEB_ACTION)
	$(TOUCH) $@
